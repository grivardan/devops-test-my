	FROM node:10-alpine
	WORKDIR /home/node/app
	COPY package*.json ./
	USER node
	COPY --chown=node:node . .
	RUN npm install
	EXPOSE 3001
	CMD [ "node", "index.js" ]	